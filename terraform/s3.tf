# resource aws_s3_bucket s3_job_offer_bucket
resource "aws_s3_bucket" "s3_job_offer_bucket" {
  bucket        = "seau-job-offer"
  force_destroy = true
  tags = {
    Name = "job offer"
  }
}

# resource aws_s3_object job_offers
resource "aws_s3_bucket_object" "job_offers" {
  depends_on = [aws_s3_bucket.s3_job_offer_bucket]
  bucket = aws_s3_bucket.s3_job_offer_bucket.id
  key    = "job_offers/"
  source = "/dev/null"
}



# resource aws_s3_bucket_notification bucket_notification
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3_job_offer_bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.s3_to_sqs_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = aws_s3_bucket_object.job_offers.key
    filter_suffix       = ".csv"
  }
  depends_on = [aws_s3_bucket_object.job_offers, aws_lambda_function.s3_to_sqs_lambda]
}





