# resource aws_sqs_queue job_offers_queue
resource "aws_sqs_queue" "job_offers_queue" {
  name = "job_offers_queue"
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.job_offers_queue_deadletter.arn,
    maxReceiveCount     = 4,
  })
}


# resource aws_sqs_queue job_offers_queue_deadletter
resource "aws_sqs_queue" "job_offers_queue_deadletter" {
  name = "dead_letter_queue"
}


# resource aws_lambda_event_source_mapping sqs_lambda_trigger

